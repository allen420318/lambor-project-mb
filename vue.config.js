"use strict"
const path = require("path")
const CopyPlugin = require("copy-webpack-plugin")
require("dotenv").config()

function resolve(dir) {
  return path.resolve(__dirname, dir)
}

function loadConfig(target, basedir = "./src/configs/") {
  const configName = target || "development"
  return basedir + configName + ".json"
}

function buildConfig(buffer) {
  const target = JSON.parse(buffer.toString())
  delete target.cdnUrl
  return JSON.stringify(target, null, 2)
}

const name = "usg" // page title
const port = 8080 // dev port

const isProduction = process.env.NODE_ENV === "production" ? true : false
const isDefaultBuild = process.env.BUILD_MODE === "default" ? true : false

const buildTarget = process.env.BUILD_TARGET

const configLocation = process.env.SITE_KEY ? `./src/mod/${process.env.SITE_KEY}/configs/` : "./src/configs/"
const configPath = loadConfig(buildTarget, configLocation)
const config = require(configPath)
const { cdnUrl } = config

const resourcePath = process.env.SITE_KEY
  ? path.resolve(__dirname, "src/mod/", process.env.SITE_KEY, "./static")
  : path.resolve(__dirname, "./static")

console.log(configPath)
console.log(process.env.SITE_KEY)

// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {
  /**
   * You will need to set publicPath if you plan to deploy your site under a sub path,
   * for example GitHub Pages. If you plan to deploy your site to https://foo.github.io/bar/,
   * then publicPath should be set to "/bar/".
   * In most cases please use '/' !!!
   * Detail: https://cli.vuejs.org/config/#publicpath
   */
  outputDir: "dist",
  assetsDir: "static",
  lintOnSave: false,
  productionSourceMap: false,
  devServer: {
    port: port,
    open: false,
    hot: true,
    overlay: {
      warnings: false,
      errors: true
    }
  },
  css: {
    loaderOptions: {
      stylus: {
        preferPathResolver: "webpack",
        import: ["~@/styles/variables/global.styl"]
      }
    }
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: name,
    resolve: {
      alias: {
        "@": resolve("src"),
        vue$: "vue/dist/vue.esm.js",
        styles: resolve("src/assets/styles"),
        scripts: resolve("src/assets/scripts"),
        images: resolve("static/images/mobile"),
        assets: resolve("src/assets"),
        statics: resolve("static")
      }
    },
    plugins: [
      new CopyPlugin({
        patterns: [
          {
            from: resourcePath,
            to: "static",
            globOptions: {
              ignore: ["**/customer/versionSet.json"]
            }
          },
          {
            from: configPath,
            to: "./static/customer/versionSet.json",
            transform(content) {
              return buildConfig(content)
            }
          }
        ]
      })
    ]
  },

  chainWebpack: (config) => {
    config.plugin("define").tap((args) => {
      args[0] = {
        ...args[0],
        SITE_KEY: `"${process.env.SITE_KEY}"`
      }

      return args
    })

    const svgRule = config.module.rule("svg")

    svgRule.uses.clear()

    svgRule.use("babel-loader").loader("babel-loader").end().use("vue-svg-loader").loader("vue-svg-loader").end()

    config.module
      .rule("jquery1")
      .test(require.resolve("jquery"))
      .use("expose-loader")
      .loader("expose-loader")
      .options("jQuery")
      .end()

    config.module
      .rule("jquery2")
      .test(require.resolve("jquery"))
      .use("expose-loader")
      .loader("expose-loader")
      .options("jquery")
      .end()

    config.module
      .rule("jquery3")
      .test(require.resolve("jquery"))
      .use("expose-loader")
      .loader("expose-loader")
      .options("$")
      .end()

    if (isProduction === true) {
      if (isDefaultBuild === true) {
        config.plugin("html").tap(([args]) => {
          const payload = { ...args, inject: false, isDefaultBuild, template: "index.html" }
          return [payload]
        })
      } else {
        config.plugin("html").tap(([args]) => {
          const payload = { ...args, inject: false, cdnUrl, isDefaultBuild, template: "index.html" }
          return [payload]
        })
      }

      config.optimization.minimizer("terser").tap((args) => {
        args[0].terserOptions.compress.drop_console = true
        return args
      })
    } else {
      config.plugin("html").tap(([args]) => {
        const payload = { ...args, inject: false, isDefaultBuild: true, template: "index.html" }
        return [payload]
      })
    }
  }
}
