export default {
    Web: {
        recharge: {
            //充值中心
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        agency: {
            // 會員 - 代理中心首页
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        profitLoss: {
            // 团队盈亏
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        userList: {
            // 用户列表
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        addUser: {
            // 用户列表
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        delUser: {
            // 用户列表
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        accountChange: {
            // 帐变明细
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        transferAccount: {
            // 转账操作
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        transfer: {
            // 转账操作
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        withdrawal: {
            // 转账操作
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        transferConfirm: {
            // 转账确认
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        promotionManagement: {
            // 推广方案
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        agentAudit: {
            // 代理审核
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        member: {
            // 會員 - 个人中心
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        transactionRecord: {
            // 會員 - 记录查询
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        extendRecord: {
            // 會員 - 推广查询
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        privateMessage: {
            // 會員 - 公告信息
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        modifyPassword: {
            // 會員 - 修改密码
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        deposit: {
            // 會員 - 充值
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        depositPaymentIosUc: {
            // 會員 - 充值
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        memberWithdrawal: {
            // 會員 - 充值
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },

        fundManage: {
            // 首页 - 钱包
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        memberDepositList: {
            // 存款记录
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        memberErawMoneyList: {
            // 取款记录
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        memberBettingList: {
            // 投注记录
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        bettingList: {
            // 投注记录
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        rechargeList: {
            // 充值记录
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        cashList: {
            // 提现记录
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        commissionList: {
            // 佣金记录
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        selfTreatment: {
            // 自身待遇
            0: {
                Ret: false,
                RedirectName: 'login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
    },
};
