import agency from "@/components/mobile/agency/index"
import member from "@/components/mobile/member/index"
import login from "@/components/mobile/login/index"

export default [
  {
    //充值中心
    name: "recharge",
    meta: {
      mdex: 1
    },
    path: "recharge",
    component: () => import("@/components/mobile/recharge/index")
  },
  {
    //添加到桌面教程
    name: "tutorial",
    meta: {
      mdex: 2
    },
    path: "tutorial",
    component: () => import("@/components/mobile/sub/tutorial/index")
  },
  {
    // 代理中心
    name: "agency",
    meta: {
      mdex: 1
    },
    path: "agency",
    component: agency
  },
  {
    // 团队盈亏
    name: "profitLoss",
    meta: {
      mdex: 2
    },
    path: "agency/profitLoss",
    component: () => import("@/components/mobile/agency/profitLoss")
  },
  {
    // 用户列表
    name: "userList",
    meta: {
      mdex: 2
    },
    path: "agency/userList",
    component: () => import("@/components/mobile/agency/userList")
  },
  {
    // 添加新会员
    name: "addUser",
    meta: {
      mdex: 3
    },
    path: "agency/addUser",
    component: () => import("@/components/mobile/sub/agency/userList/addUser")
  },
  {
    // 回收代理
    name: "delUser",
    meta: {
      mdex: 3
    },
    path: "agency/delUser",
    component: () => import("@/components/mobile/sub/agency/userList/delUser")
  },
  {
    // 帐变明细
    name: "accountChange",
    meta: {
      mdex: 2
    },
    path: "agency/accountChange",
    component: () => import("@/components/mobile/agency/accountChange")
  },
  {
    // 转账操作
    name: "transferAccount",
    meta: {
      mdex: 2
    },
    path: "agency/transferAccount",
    component: () => import("@/components/mobile/agency/transferAccount")
  },
  {
    // 转账操作-转账
    name: "transfer",
    meta: {
      mdex: 3
    },
    path: "agency/transfer",
    component: () => import("@/components/mobile/sub/agency/transferAccount/transfer")
  },
  {
    // 转账操作-取款
    name: "withdrawal",
    meta: {
      mdex: 3
    },
    path: "agency/withdrawal",
    component: () => import("@/components/mobile/sub/agency/transferAccount/withdrawal")
  },
  {
    // 转账操作-确认
    name: "transferConfirm",
    meta: {
      mdex: 3
    },
    path: "agency/transferConfirm",
    component: () => import("@/components/mobile/sub/agency/transferAccount/transferConfirm")
  },
  {
    // 推广方案
    name: "promotionManagement",
    meta: {
      mdex: 2
    },
    path: "agency/promotionManagement",
    component: () => import("@/components/mobile/agency/promotionManagement")
  },
  {
    // 会员二维码
    name: "promotionQRcode",
    meta: {
      mdex: 3
    },
    path: "agency/promotionQRcode",
    component: () => import("@/components/mobile/sub/agency/promotionPlan/promotionQRcode")
  },
  {
    // 代理审核
    name: "agentAudit",
    meta: {
      mdex: 2
    },
    path: "agency/agentAudit",
    component: () => import("@/components/mobile/agency/agentAudit")
  },
  {
    // 投注记录
    name: "bettingList",
    meta: {
      mdex: 2
    },
    path: "agency/bettingList",
    component: () => import("@/components/mobile/agency/bettingList")
  },
  {
    // 充值记录
    name: "rechargeList",
    meta: {
      mdex: 2
    },
    path: "agency/rechargeList",
    component: () => import("@/components/mobile/agency/rechargeList")
  },
  {
    // 提现记录
    name: "cashList",
    meta: {
      mdex: 2
    },
    path: "agency/cashList",
    component: () => import("@/components/mobile/agency/cashList")
  },
  {
    // 佣金记录
    name: "commissionList",
    meta: {
      mdex: 2
    },
    path: "agency/commissionList",
    component: () => import("@/components/mobile/agency/commissionList")
  },
  {
    // 自身待遇
    name: "selfTreatment",
    meta: {
      mdex: 2
    },
    path: "agency/selfTreatment",
    component: () => import("@/components/mobile/agency/selfTreatment")
  },
  {
    //个人中心
    name: "member",
    meta: {
      mdex: 1
    },
    path: "member",
    component: member
  },
  {
    // 账变记录
    name: "memberAccountChange",
    meta: {
      mdex: 2
    },
    path: "member/memberAccountChange",
    component: () => import("@/components/mobile/member/accountChange")
  },
  {
    //充值
    name: "deposit",
    meta: {
      mdex: 2
    },
    path: "member/deposit",
    component: () => import("@/components/mobile/member/deposit_new")
  },
  {
    //在线支付
    name: "depositPayment",
    meta: {
      mdex: 3
    },
    path: "member/depositPayment/:Data",
    component: () => import("@/components/mobile/member/depositPayment")
  },
  {
    //在线支付
    name: "depositPaymentIosUc",
    meta: {
      mdex: 3
    },
    path: "member/depositPaymentIosUc/:Data",
    component: () => import("@/components/mobile/member/depositPaymentIosUc")
  },
  {
    //在线支付
    name: "memberWithdrawal",
    meta: {
      mdex: 3
    },
    path: "member/withdraw",
    component: () => import("@/components/mobile/member/withdrawal")
  },
  {
    //记录查询
    name: "transactionRecord",
    meta: {
      mdex: 2
    },
    path: "member/transactionRecord",
    component: () => import("@/components/mobile/member/transactionRecord")
  },
  {
    //推广查询
    name: "extendRecord",
    meta: {
      mdex: 2
    },
    path: "member/extendRecord",
    component: () => import("@/components/mobile/member/extendRecord")
  },
  {
    //公告信息
    name: "privateMessage",
    meta: {
      mdex: 2
    },
    path: "member/privateMessage",
    component: () => import("@/components/mobile/member/privateMessage")
  },
  {
    //修改密码
    name: "modifyPassword",
    meta: {
      mdex: 2
    },
    path: "member/modifyPassword",
    component: () => import("@/components/mobile/member/modifyPassword")
  },
  {
    //资金管理
    name: "fundManage",
    meta: {
      mdex: 2
    },
    path: "member/fundManage",
    component: () => import("@/components/mobile/member/fundManage")
  },
  {
    // 存款记录
    name: "memberDepositList",
    meta: {
      mdex: 2
    },
    path: "member/memberDepositList",
    component: () => import("@/components/mobile/member/depositList")
  },
  {
    // 取款记录
    name: "memberErawMoneyList",
    meta: {
      mdex: 2
    },
    path: "member/memberErawMoneyList",
    component: () => import("@/components/mobile/member/erawMoneyList")
  },
  {
    // 投注记录
    name: "memberBettingList",
    meta: {
      mdex: 2
    },
    path: "member/memberBettingList",
    component: () => import("@/components/mobile/member/bettingList")
  },
  {
    //登录
    name: "login",
    meta: {
      mdex: 1
    },
    path: "login",
    component: login
  },
  {
    //微信登录,带推广链接
    name: "author",
    meta: {
      mdex: 1
    },
    path: "author/:DomainLink?",
    component: () => import("@/components/mobile/login/author")
  },
  {
    //微信授权获取code
    name: "code",
    meta: {
      mdex: 1
    },
    path: "code/:DomainLink?",
    component: () => import("@/components/mobile/login/code")
  },

  {
    //注册
    name: "register",
    meta: {
      mdex: 1
    },
    path: "register/:Data?",
    component: () => import("@/components/mobile/register/index")
  },
  {
    //微信开通平台账密
    name: "registerSuccess",
    meta: {
      mdex: 3
    },
    path: "registerSuccess",
    component: () => import("@/components/mobile/register/registerSuccess")
  },
  {
    //微信开通平台账密
    name: "openPlatform",
    meta: {
      mdex: 1
    },
    path: "openPlatform",
    component: () => import("@/components/mobile/openPlatform/index")
  },
  {
    // 所有遊戲畫面
    name: "playGame",
    meta: {
      mdex: 1
    },
    path: "playGame",
    component: () => import("@/components/mobile/linkToGame/playGame")
  },
  {
    // PNG游戏界面
    name: "playPngGame",
    meta: {
      mdex: 1
    },
    path: "playPngGame",
    component: () => import("@/components/mobile/linkToGame/playPngGame")
  },
  {
    // 游戏界面--返回首页
    name: "playGameBackIndex",
    meta: {
      mdex: 1
    },
    path: "playGameBackIndex",
    component: () => import("@/components/mobile/linkToGame/playGameBackIndex")
  },
  {
    // 调整qq客服
    name: "linkToQQ",
    meta: {
      mdex: 1
    },
    path: "linkToQQ",
    component: () => import("@/components/mobile/sub/service/linkToQQ")
  },
  {
    // 活动
    name: "activity",
    meta: {
      mdex: 1
    },
    path: "activity",
    component: () => import("@/components/mobile/activity/index")
  },
  {
    // 活动详情
    name: "activityInfo",
    meta: {
      mdex: 2
    },
    path: "activity/info",
    component: () => import("@/components/mobile/activity/activeInfo")
  },
  {
    // 加盟我们
    name: "JoinUs",
    meta: {
      mdex: 1
    },
    path: "JoinUs/:Data",
    component: () => import("@/components/mobile/JoinUs/index")
  },
  {
    // 游戏列表
    name: "eGameList",
    meta: {
      mdex: 0
    },
    path: "eGameList/:Data",
    component: () => import("@/components/mobile/eGameList/index")
  },
  {
    // 棋牌游戏大厅
    name: "pokerGameLobby",
    meta: {
      mdex: 0
    },
    path: "pokerGameLobby/:Data",
    component: () => import("@/components/mobile/pokerGameLobby/index")
  }
]
