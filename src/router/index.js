import Vue from "vue"
import Router from "vue-router"
import PageRedirect from "@/router/redirect"

import "@/components/mobile/index/index"
import CommonService from "scripts/businessLogic/commonService"
import routerSwitch from "@/routerSwitch.js"
import store from "@/store/index.js"
import { isMobileAgent } from "@/libs/navigator.js"
import { getLanguageCode } from "@/lang/helper.js"

Vue.use(Router)

const router = new Router({
  mode: "history",
  routes: routerSwitch
})

router.beforeEach(async (to, from, next) => {
  if (isMobileAgent() === false) {
    location.replace(Vue.prototype.$WebVersionHost + to.fullPath)
    return
  }

  const versionSetLanguageCode = Vue.prototype.$Lang
  const currentLanguageCode = getLanguageCode(versionSetLanguageCode)

  store.dispatch("i18n/loadLanguage", { languageCode: currentLanguageCode })

  // 加時間防止被登出
  localStorage.setItem("logoutTime", new Date().getTime() + 15 * 60 * 1000)

  store.dispatch("user/setAgentId")
  store.dispatch("user/setUserId")

  // 確認是否需檢查路由
  if (PageRedirect.Web[to.name] !== undefined) {
    const data = await CommonService.Comm_CheckPermission()

    // 避免Token殘留
    if (data.Status === 0) {
      localStorage.removeItem("Token")
    }

    const redirectData = PageRedirect.Web[to.name][data.Status]
    // 根據登入狀態判斷是否合法路由，若非法則進入指定路由
    if (redirectData.Ret === true) {
      // 无代理身份和停用代理后不得进入代理中心 2019.12.18 ququ
      if (/^\/agency/.test(to.path)) {
        const IBStatus = localStorage.getItem("IBStatus")
        const CorrelationIBMemberId = localStorage.getItem("CorrelationIBMemberId")
        if (IBStatus === "703" || (IBStatus !== 2 && CorrelationIBMemberId === "-1")) {
          next("/")
        } else {
          next()
        }
      } else {
        next()
      }
    } else {
      next({
        name: redirectData.RedirectName
      })
    }
  } else {
    next()
  }
})

router.afterEach(function () {
  // 跳轉頁面後回歸頂部
  document.body.scrollTop = 0
  document.documentElement.scrollTop = 0
})

export default router
