export default {
  data: () => ({
    timeCounterLabel: undefined,
    rafID: "",
    endTime: ""
  }),
  methods: {
    getCountdownDuration({ minutes } = {}) {
      const m = minutes ? minutes : 0
      const time = m * 60000
      return new Date().getTime() + time
    },
    getRemainingTime(endTime) {
      const currentTime = new Date().getTime()
      return endTime - currentTime
    },
    startCountdown(endTime, { isDisplayHours = true, isDisplayMinutes = true, isDisplaySeconds = true } = {}) {
      const config = { isDisplayHours, isDisplayMinutes, isDisplaySeconds }
      return this.boom(endTime, config)
    },
    clearCountdown() {
      this.endTime = 0
      cancelAnimationFrame(this.rafID)
      this.rafID = ""
      this.timeCounterLabel = ""
    },
    boom(endTime, { isDisplayHours = true, isDisplayMinutes = true, isDisplaySeconds = true } = {}) {
      const config = { isDisplayHours, isDisplayMinutes, isDisplaySeconds }
      this.endTime = endTime
      this.rafID = requestAnimationFrame((x) => {
        this.startCount(x, config)
      })
    },
    timeCountFormat(timestamp, { isDisplayHours = true, isDisplayMinutes = true, isDisplaySeconds = true } = {}) {
      const h = isDisplayHours === true ? this.fillzero(Math.floor(timestamp / 3600000) % 24) : ""
      const m = isDisplayMinutes === true ? this.fillzero(Math.floor(timestamp / 60000) % 60) : ""
      const s = isDisplaySeconds === true ? this.fillzero(Math.floor(timestamp / 1000) % 60) : ""

      const result = [h, m, s].filter((x) => x !== "")
      this.timeCounterLabel = result.join(":")
    },
    fillzero(number) {
      if (number > 0) {
        if (number > 9) {
          return number
        } else {
          return `0${number}`
        }
      } else {
        return "00"
      }
    },
    startCount(x, config) {
      const count = this.getRemainingTime(this.endTime)

      this.timeCountFormat(count, config)

      if (count >= 0) {
        this.rafID = requestAnimationFrame((x) => {
          this.startCount(x, config)
        })
      } else {
        this.clearCountdown()
      }
    }
  }
}
