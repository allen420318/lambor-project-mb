export const fixInputBugForIOS = () => {
  if (/iphone|ipod|ipad/i.test(navigator.appVersion)) {
    document.addEventListener(
      "blur",
      event => {
        if (
          document.documentElement.offsetHeight <= document.documentElement.clientHeight &&
          ["input", "textarea"].includes(event.target.localName)
        ) {
          document.body.scrollIntoView()
        }
      },
      true
    )
  }
}
