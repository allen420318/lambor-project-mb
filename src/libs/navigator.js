export const isMobileAgent = () => {
  return window.navigator.userAgent.toLowerCase().includes("mobi")
}
