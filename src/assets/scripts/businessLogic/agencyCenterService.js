import HttpService from 'scripts/common/HttpService';
import BLDef from 'scripts/common/BLDef';

export default {

    // 代理中心
    async AddIB_LoadMainPage_Mgr(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AddIB_LoadMainPage, obj);
        return retData;
    },
    // 获取上级设定 返回最高佣金
    async LoadAddIBSettingUpperIBSetting(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AddIB_LoadUpperIBSetting, obj);
        return retData;
    },
    // 获取上级设定的最高上缴比 返回红利、晋升制等
    async LoadMaxReverseGameListSetting(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.LoadMaxReverseGameListSetting, obj);
        return retData;
    },
    async LoadAddIB_Add(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AddIB_Add, obj);
        return retData;
    },
    async LoadTempCommissionRevenueGameSettingPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.LoadTempCommissionRevenueGameSetting, obj);
        return retData;
    },
    async AgencyRechargeList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.GetIBDepositList, obj);
        return retData;
    },
    async AgencyRechargeAccountList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.GetMemberDepositList, obj);
        return retData;
    },
    async IBUserList_Query(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBUserList_Query, obj);
        return retData;
    },
    async TeamProfitAndLoss_LoadMainPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.TeamProfitAndLoss_LoadMainPage, obj);
        return retData;
    },
    async TeamVariousTotal_LoadMainPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.TeamVariousTotal_LoadMainPage, obj);
        return retData;
    },
    async BrokerWalletCheckDetails_LoadDetailPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.BrokerWalletCheckDetails_LoadDetailPage, obj);
        return retData;
    },
    async AgentTeamTotal_LoadDetailPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AgentTeamTotal_LoadDetailPage, obj);
        return retData;
    },
    async PersonalProfitAndLoss_LoadDetailPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.PersonalProfitAndLoss_LoadDetailPage, obj);
        return retData;
    },

    // <summary> 获取用户下的直属站长与直属代理 </summary>
    async LoadAuthorizeUserList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.LoadAuthorizeUserList, obj);
        return retData;
    },
    // <summary> 获取游戏金额授权记录列表 </summary>
    async AuthorizeUserTransactionList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AuthorizeUserTransactionList, obj);
        return retData;
    },
    // <summary> 新增游戏金额授权记录列表 </summary>
    async AuthorizeUserTransactionAdd(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AuthorizeUserTransactionAdd, obj);
        return retData;
    },
    // / <summary>团队总计图表</summary>
    async AgentTeamTotalStatistical_LoadDetailPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AgentTeamTotalStatistical_LoadDetailPage, obj);
        return retData;
    },
    // / <summary>人数统计图表</summary>
    async TeamVariousTotalStatistical_LoadMainPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.TeamVariousTotalStatistical_LoadMainPage, obj);
        return retData;
    },
    // <summary>查询转账记录</summary>
    async TransferOperation_Query(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.TransferOperation_Query, obj);
        return retData;
    },
    // <summary> 根据当前用户获取用户余额 </summary>
    async LoadMemberBalance_FrontEnd() {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.LoadMemberBalance_FrontEnd);
        return retData;
    },

    // <summary>验证用户名</summary>
    async LoadVerificationLoginID(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.VerificationLoginID, obj);
        return retData;
    },
    // <summary>一键回收</summary>
    async LoadIBUserListRecycle(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBUserList_Recycle, obj);
        return retData;
    },
    // <summary>新增方案页面载入</summary>
    async LoadPageIBExtensionPlanAdd(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExtensionPlanAdd_LoadPage, obj);
        return retData;
    },
    // <summary>代理审核列表</summary>
    async IBExamineList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExamineList, obj);
        return retData;
    },
    // <summary>代理审核详细页面</summary>
    async IBExamineDetail(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExamineDetail, obj);
        return retData;
    },
    // <summary>代理审核详细页面</summary>
    async IBExamineRefuse(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExamineRefuse, obj);
        return retData;
    },
    // <summary>方案列表载入</summary>
    async IBExtensionPageLoad(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExtensionPageLoad, obj);
        return retData;
    },
    // <summary>新增方案</summary>
    async IBExtensionPlanAdd(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExtensionPlanAdd, obj);
        return retData;
    },
    // <summary>编辑方案页面载入</summary>
    async IBExtensionPlanMod_LoadPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExtensionPlanMod_LoadPage, obj);
        return retData;
    },
    // <summary>编辑方案</summary>
    async IBExtensionPlanMod(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExtensionPlanMod, obj);
        return retData;
    },
    // <summary>删除方案</summary>
    async IBExtensionPlanDel(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBExtensionPlanDel, obj);
        return retData;
    },
    // <summary>用户列表请求成为代理</summary>
    async LoadIBUserListNoRequest(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBUserList_NoRequest, obj);
        return retData;
    },
    // <summary>会员提现列表</summary>
    async LoadWithdrawalList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.WithdrawalList, obj);
        return retData;
    },
    // <summary>会员提现明细</summary>
    async LoadWithdrawalDetail(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.WithdrawalDetail, obj);
        return retData;
    },
    //  <summary>上级代理列表</summary>
    async LoadIBList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.IBList, obj);
        return retData;
    },
    //  <summary>新增会员</summary>
    async LoadMemberAdd(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.MemberAdd, obj);
        return retData;
    },
    //  <summary>账变明细-占成</summary>
    async LoadRakeCostList(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.RakeCostList, obj);
        return retData;
    },
    //  <summary>账变明细-投注</summary>
    async LoadGetIBBetReportQuery(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.GetIBBetReport_Query, obj);
        return retData;
    },
    //  <summary>账变明细-码佣</summary>
    async LoadGetIBCommissionReportQuery(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.GetIBCommissionReport_Query, obj);
        return retData;
    },
    //  <summary>账变明细-投注报表</summary>
    async LoadGameRecord_LoadMainPage(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.GameRecord_LoadMainPage, obj);
        return retData;
    },
    //  <summary>代理-团队码佣-查看详细</summary>
    async LoadGetIBCommissionReport_Details(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.GetIBCommissionReport_Details, obj);
        return retData;
    },
    // 查詢投注記錄
    async Record_Activity(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.Record_Activity, obj);
        return retData;
    },
    // 查詢投注記錄
    async AccountChangeDetails_Query(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.AccountChangeDetails_Query, obj);
        return retData;
    },
    // 交易类型
    async Record_TransType(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.Record_TransType, obj);
        return retData;
    },
    // 投注记录-游戏厂商
    async LoadGameTypeAndVendor(obj) {
        const retData = await HttpService.PostAes(BLDef.IBService.ServiceType, BLDef.IBService.ActType.GameRecordLoadMainPage_Details, obj);
        return retData;
    },
};
