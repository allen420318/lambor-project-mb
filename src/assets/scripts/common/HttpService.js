import axios from "axios"
import Vue from "vue"
import Security from "@/assets/scripts/common/Security.js"
import store from "@/store/index.js"
import { isTriggerDebugLogger, apiLogger } from "@/libs/logger.js"
import { fetcher } from "@/libs/fetcher.js"
import { languageCodeMap } from "@/lang/helper.js"
import i18n from "@/lang/i18nLoader.js"

const defaultSitePlatformTypeCode = store.state.site.platformTypeCode

const setLogoutTime = () => {
  const aliveMinutes = 15
  localStorage.setItem("logoutTime", new Date().getTime() + aliveMinutes * 60 * 1000)
}

const getLanguageId = () => {
  const [target] = [...languageCodeMap].filter(([, v]) => v === i18n.locale).map((x) => x[0]) || 0

  return target
}

// 動態加解密request
export const PostDynamic = async (
  ST,
  AT,
  DataObj = null,
  customToken,
  platformTypeCode = defaultSitePlatformTypeCode
) => {
  setLogoutTime()

  const token = customToken ? customToken : localStorage.getItem("Token")

  const savedPayload = {
    endpoint: Vue.prototype.$BKBLUrl,
    AT: AT,
    ST: ST,
    TK: token,
    LG: getLanguageId(),
    Platform: platformTypeCode,
    Data: DataObj
  }

  const payload = {
    ...savedPayload,
    Data: Security.DynamicEncryptWithBase64(DataObj, Security.KeyGenSNKey(1), Security.KeyGenGuidKey("1"))
  }

  try {
    const result = await fetcher(payload)

    const decryptResult = Security.DynamicDecryptToObj(result.Rep)

    if (isTriggerDebugLogger() === true) {
      const apiData = {
        st: ST,
        at: AT,
        result: decryptResult,
        payload: {
          ...savedPayload,
          Data: DataObj
        }
      }

      apiLogger(apiData)
    }

    if (decryptResult) {
      return decryptResult
    } else {
      return {
        Ret: "-1",
        Message: i18n.t("lang.ACCOUNT_EXPIRED"),
        Data: null
      }
    }
  } catch (err) {
    if (isTriggerDebugLogger() === true) {
      const apiData = { st: ST, at: AT, result: err }

      apiLogger(apiData, false)
    }

    return {
      Ret: "-1",
      Message: "PostDynamic Response Error",
      Data: null
    }
  }
}

// AES加密request
export const PostAes = async (ST, AT, DataObj = null) => {
  setLogoutTime()

  const key = localStorage.getItem("Auth")

  const savedPayload = {
    endpoint: Vue.prototype.$BKBLUrl,
    ST: ST,
    AT: AT,
    TK: localStorage.getItem("Token"),
    LG: getLanguageId()
  }

  const payload = {
    ...savedPayload,
    Data: Security.AESEncryptWithBase64(JSON.stringify(DataObj), key)
  }

  try {
    const result = await fetcher(payload)

    const decryptResult = Security.AESDecryptToObj(result.Rep, key)

    if (isTriggerDebugLogger() === true) {
      const apiData = {
        st: ST,
        at: AT,
        result: decryptResult,
        payload: {
          ...savedPayload,
          Data: DataObj
        }
      }

      apiLogger(apiData)
    }

    if (decryptResult) {
      return decryptResult
    } else {
      return {
        Ret: "-1",
        Message: i18n.t("lang.ACCOUNT_EXPIRED"),
        Data: null
      }
    }
  } catch (err) {
    if (isTriggerDebugLogger() === true) {
      const apiData = {
        st: ST,
        at: AT,
        result: err,
        payload: {
          ...savedPayload,
          Data: DataObj
        }
      }

      apiLogger(apiData, false)
    }

    return {
      Ret: "-1",
      Message: "PostAes Response Error",
      Data: null
    }
  }
}

// http get html內容
export const GetContent = (serviceUrl) => {
  return axios({
    url: serviceUrl,
    dataType: "text"
  }).then(
    function success(rsp) {
      console.log(rsp.data)
      return rsp.data
    },
    function error() {
      return "GetContent Response Error"
    }
  )
}

// 匯出Excel
const ExportExcel = (exportFileName, ST, AT, DataObj) => {
  if (DataObj == undefined) {
    DataObj = null
  }
  const key = localStorage.getItem("Auth")

  return axios
    .post(
      Vue.prototype.$BKBLUrl,
      {
        LG: getLanguageId(),
        ST: ST,
        AT: AT,
        TK: localStorage.getItem("Token"),
        Data: Security.AESEncryptWithBase64(JSON.stringify(DataObj), key)
      },
      {
        responseType: "arraybuffer"
      }
    )
    .then((rsp) => {
      const filename = exportFileName + ".xlsx"
      const contentType = rsp.headers["content-type"]

      try {
        const blob = new Blob([rsp.data], {
          type: contentType
        })

        if (window.navigator.msSaveOrOpenBlob) {
          // for IE
          window.navigator.msSaveOrOpenBlob(blob, filename)
        } else {
          const url = window.URL.createObjectURL(blob)
          const linkElement = document.createElement("a")
          linkElement.setAttribute("href", url)
          linkElement.setAttribute("download", filename)

          const clickEvent = new MouseEvent("click", {
            view: window,
            bubbles: true,
            cancelable: false
          })

          linkElement.dispatchEvent(clickEvent)
        }
      } catch (ex) {
        console.log(ex)
      }
    })
}

export default {
  PostDynamic,
  PostAes,
  GetContent,
  ExportExcel
}
