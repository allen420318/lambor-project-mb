import moment from "moment"
import BLDef from "@/assets/scripts/common/BLDef"
import URLService from "@/assets/scripts/common/URLService"
import GameService from "@/assets/scripts/businessLogic/gameService"
import PersonalCenterService from "@/assets/scripts/businessLogic/personalCenterService"
import CommonService from "@/assets/scripts/businessLogic/commonService"
import EventBus from "@/assets/scripts/common/EventBus"
import Router from "@/router"
import Vue from "vue"

export default {
  // 計算總資產金額
  async CalculateSummaryAmount(that) {
    let retSummaryAmount = 0
    let retBool = false
    const data = await GameService.GetGameMaintainStatus()

    if (data.Ret == 0) {
      for (let i = 0; i < data.Data.GameAPIVendorMaintainStatusList.length; i++) {
        if (data.Data.GameAPIVendorMaintainStatusList[i].IsMaintaining === "702") {
          const inputObj = {
            APIVendorID: data.Data.GameAPIVendorMaintainStatusList[i].GameAPIVendor
          }
          // eslint-disable-next-line no-await-in-loop
          const balanceData = await PersonalCenterService.EWalletTransfer_GetBalance(inputObj)
          if (balanceData.Ret == 0) {
            retSummaryAmount = this.NumberAdd(retSummaryAmount, balanceData.Data.GameApiBalance.Amount)
            retBool = true
          }
        }
      }

      if (retBool === "703") {
        retSummaryAmount = that.$t("lang.GAME_MAINTENANCE")
      }
    } else {
      retSummaryAmount = that.$t("lang.GAME_MAINTENANCE")
    }
    return retSummaryAmount
  },
  tmpScrollHeight: undefined,
  // 自訂加法器
  NumberAdd(arg1, arg2) {
    let r1
    let r2
    let m = 0
    let c = 0
    try {
      r1 = arg1.toString().split(".")[1].length
    } catch (e) {
      r1 = 0
    }
    try {
      r2 = arg2.toString().split(".")[1].length
    } catch (e) {
      r2 = 0
    }
    c = Math.abs(r1 - r2)
    //eslint-disable-next-line
    m = Math.pow(10, Math.max(r1, r2))
    if (c > 0) {
      //eslint-disable-next-line
      const cm = Math.pow(10, c)
      if (r1 > r2) {
        arg1 = Number(arg1.toString().replace(".", ""))
        arg2 = Number(arg2.toString().replace(".", "")) * cm
      } else {
        arg1 = Number(arg1.toString().replace(".", "")) * cm
        arg2 = Number(arg2.toString().replace(".", ""))
      }
    } else {
      arg1 = Number(arg1.toString().replace(".", ""))
      arg2 = Number(arg2.toString().replace(".", ""))
    }
    return (arg1 + arg2) / m
  },
  GenGuid() {
    let d = new Date().getTime()
    const uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
      const r = (d + Math.random() * 16) % 16 | 0
      d = Math.floor(d / 16)
      return (c === "x" ? r : (r & 0x3) | 0x8).toString(16)
    })
    return uuid
  }, // end GenGuid

  GenRandomInteger(MinNum, MaxNum) {
    // 取得 minNum(最小值) ~ maxNum(最大值) 之間的亂數
    return Math.floor(Math.random() * (MinNum - MaxNum + 1)) + MinNum
  }, // end GenRandomInteger

  // 随机生成用户注册名
  RandomAcct() {
    let acct = "" //格式：XX0000
    var randomX = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9) //数字随机数
    var randomY = new Array(
      "a",
      "b",
      "c",
      "d",
      "e",
      "f",
      "g",
      "h",
      "i",
      "j",
      "k",
      "l",
      "m",
      "n",
      "o",
      "p",
      "q",
      "r",
      "s",
      "t",
      "u",
      "v",
      "w",
      "x",
      "y",
      "z"
    ) //字母随机数
    for (var i = 0; i < 2; i++) {
      //循环操作
      var index = Math.floor(Math.random() * 26) //取得随机数的索引（a~y）
      acct += randomY[index]
    }
    for (var i = 0; i < 4; i++) {
      //循环操作   b
      var index = Math.floor(Math.random() * 10) //取得随机数的索引（0~9）
      acct += randomX[index]
    }
    return acct
  },

  // tick轉換為本地時間
  UtcTicksToLocalTime(utcDateTicks, format) {
    if (!utcDateTicks) {
      return undefined
    }
    return moment(utcDateTicks)
      .utcOffset(8 * 60)
      .format(format)
  },

  // tick轉換為时分秒
  UtcTicksTime(utcDateTicks, format) {
    if (!utcDateTicks) {
      return undefined
    }
    return moment(utcDateTicks)
      .utcOffset(0 * 60)
      .format(format)
  },

  // 本地時間轉換為tick
  LocalTimeToUtcTicks(orgLocalDateTime, IsEndTime, IsContainTime) {
    if (!orgLocalDateTime) {
      return undefined
    }

    let localDateTime
    if (IsContainTime === true) {
      localDateTime = orgLocalDateTime
    } else {
      // 將時分秒格式去除, 保留日期部份避免受當地時間影響
      localDateTime = moment(orgLocalDateTime).format("YYYY/MM/DD")
    }
    // 計算與UTC+8的時差
    const timeoffset = (new Date().getTimezoneOffset() / 60 + 8) * -1

    // 補正同一目標日期時間，與UTC+8差距的tick
    const utcEightFixTick = timeoffset * 3600 * 1000

    if (IsEndTime === true) {
      return new Date(localDateTime).getTime() + utcEightFixTick + (86400 * 1000 - 1) // 結束日期以當天的23:59:59.999
    }

    return new Date(localDateTime).getTime() + utcEightFixTick
  },

  // ios12.0以上版本 input事件 键盘跟着页面上去，页面热键错乱
  ToScrollTop: function () {
    $("input[type='text'],input[type='password'],input[type='date'],select").blur(function () {
      window.scrollTo(0, 0) //页面滚动到顶部
    })
  },

  // 手機格式隱藏部分號碼
  PhoneNumberFormat(phoneNumber) {
    if (!phoneNumber) {
      return phoneNumber
    }

    const phoneNumberString = phoneNumber.toString()
    const phoneNumberLength = phoneNumberString.length
    if (phoneNumberLength <= 4) {
      return phoneNumberString
    }

    const tmpArray = phoneNumberString.split("")
    if (phoneNumberLength <= 8) {
      let toAddChars = ""
      for (let index = 0; index < phoneNumberLength - 4; index += 1) {
        toAddChars += "*"
      }
      tmpArray.splice(0, phoneNumberLength - 4, toAddChars)
    } else {
      tmpArray.splice(phoneNumberLength - 8, 4, "****")
    }
    return tmpArray.join("")
  },

  // 金錢轉換格式
  MoneyFormat(content) {
    if (content !== undefined && !isNaN(parseFloat(content)) && isFinite(content)) {
      // return '¥' + content.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
      return content.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    return content
  },

  // 性別轉換格式
  SexFormat(content) {
    if (content !== undefined) {
      switch (Number(content)) {
        case BLDef.SexType.SexType_Undefined:
          return ""
        case BLDef.SexType.SexType_Male:
          return "男"
        case BLDef.SexType.SexType_Female:
          return "女"
        default:
          return undefined
      }
    }
    return content
  },

  // 僅顯示內容前幾位
  LimitTo(content, digit) {
    return content.substring(0, digit)
  },

  // 簡化顯示字元
  SimpleContent(orgText, displayLength) {
    // 過濾html標籤
    const tmp = document.createElement("DIV")
    tmp.innerHTML = orgText
    const pureText = tmp.textContent || tmp.innerText || ""
    // 過濾ASCII不可見字元
    let finalText = pureText.replace(/[\x00-\x1F]/g, "")
    // 純字串若長度超過則省略
    if (finalText.length > displayLength) {
      finalText = finalText.substring(0, displayLength) + "..."
    }
    return finalText
  },

  // 超過的數字顯示..+
  UpperLimitDisplay(content, limitNumber) {
    if (content !== undefined && isNaN(content) === false && content > limitNumber) {
      return limitNumber + "+"
    }
    return content
  },

  // 顯示特殊彈窗
  WebShowUniqueForm(ID) {
    $("#main_section").css("z-index", "10086")
    $("#" + ID).show()
    this.MobileDisableScroll()
  },

  // 關閉特殊彈窗
  WebCloseUniqueForm(ID) {
    $("#" + ID).hide()
    $("#main_section").css("z-index", "10")
    this.MobileEnableScroll()
  },

  // 顯示訊息彈窗
  WebShowMessageForm() {
    $("#MessageWindow").show()
    this.MobileDisableScroll()
  },

  // 關閉訊息彈窗
  WebCloseMessageForm() {
    $("#MessageWindow").hide()
    this.MobileEnableScroll()
  },

  // 手機防止畫面滾動
  MobileDisableScroll() {
    this.tmpScrollHeight = $(window).scrollTop()
    $("body").css("position", "fixed")
    $("body").css("width", "100%")
  },

  // 手機恢復畫面滾動
  MobileEnableScroll() {
    $("body").css("position", "static")
    $("body").css("width", "auto")
    $(window).scrollTop(this.tmpScrollHeight)
  },

  // 另開統一的遊戲視窗
  OpenPlayGameWindow(routerName, windowName, Data) {
    let url = ""
    if (Data == undefined) {
      url = Router.resolve({
        name: routerName
      }).href
    } else {
      url = Router.resolve({
        name: routerName,
        params: {
          Data: URLService.GetUrlParameterFromObj(Data)
        }
      }).href
    }
    Vue.prototype.$DepositWindow = window.open(url, windowName, "width=1500,height=800,left=10,top=150")
  }, // end OpenPlayGameWindow

  // 綁定瀑布流事件
  BindWaterfallTable(scrollTriggerPercent, callbackFunction) {
    $(window).scroll(function () {
      if ($(window).scrollTop() + $(window).height() > ($(document).height() * scrollTriggerPercent) / 100) {
        callbackFunction()
      }
    })
  },

  // 代理中心綁定瀑布流事件
  AgentBindWaterfallTable(scrollTriggerPercent, callbackFunction) {
    $("#wrapper").scroll(function () {
      // console.log($('#wrapper').scrollTop() + $('#wrapper').height() > $('#wrapper').get(0).scrollHeight * 80 / 100);
      // console.log($('#wrapper').scrollTop() + $('#wrapper').height());
      // console.log($('#wrapper').get(0).scrollHeight);
      if ($("#wrapper").scrollTop() + $("#wrapper").height() > ($("#wrapper").get(0).scrollHeight * 80) / 100) {
        callbackFunction()
      }
    })
  },

  // 綁定按下Enter事件
  BindEnterTrigger(TriggerElement, TriggerFunction) {
    $("#" + TriggerElement).keypress(function (e) {
      if (e.target.nodeName == "TEXTAREA") {
        return
      }

      if (e.which == 13) {
        TriggerFunction()
        $(":focus").blur()
      }
    })
  },

  IsWeiXin() {
    //window.navigator.userAgent属性包含了浏览器类型、版本、操作系统类型、浏览器引擎类型等信息，这个属性可以用来判断浏览器类型
    var ua = window.navigator.userAgent.toLowerCase()
    //通过正则表达式匹配ua中是否含有MicroMessenger字符串
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
      return true
    } else {
      return false
    }
  },

  // 设置 cookie 值的函数
  SetCookie(cname, cvalue, exdays) {
    const d = new Date()
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000)
    const expires = "expires=" + d.toGMTString()
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
  },

  // 获取 cookie 值的函数
  GetCookie(cname) {
    const name = cname + "="
    const ca = document.cookie.split(";")
    for (let i = 0; i < ca.length; i++) {
      const c = ca[i].trim()
      if (c.indexOf(name) == 0) return c.substring(name.length, c.length)
    }
    return ""
  },
  // 畫面大小調整時觸發 20191015 ququ
  ResetOrientation: function () {
    ;(function (doc, win) {
      //此处是兼容ios的写法
      let supportOrientation = typeof window.orientation === "number" && typeof window.onorientationchange === "object",
        oFlag = false,
        ow,
        lFlag = false,
        lh
      // oFlag = false,ow,lFlag = false,lh 记住第一次的值就好了 不让fontsize因为计算单位发生变化而改变
      let init = function () {
        let orientation,
          docEl = doc.documentElement,
          dpr = Math.min(win.devicePixelRatio, 3),
          count = 0
        let updateOrientation = function () {
          let clientWidth = docEl.clientWidth
          let clientHeight = docEl.clientHeight
          if (supportOrientation) {
            orientation = window.orientation
            switch (orientation) {
              case 90:
              case -90:
                orientation = "landscape" // 横屏
                break
              default:
                orientation = "portrait" // 竖屏
                break
            }
          } else {
            orientation = window.innerWidth > window.innerHeight ? "landscape" : "portrait"
          }
          docEl.setAttribute("class", orientation)
          if (!oFlag) {
            if (clientWidth > clientHeight) {
              ow = clientHeight
            } else {
              ow = clientWidth
            }
          }
          if (!lFlag) {
            if (clientHeight > clientWidth) {
              lh = clientWidth
              lh = clientWidth
            } else {
              lh = clientHeight
            }
          }

          if (orientation === "portrait") {
            // 竖屏
            if (clientWidth == undefined) return
            if (clientWidth / dpr > 750) {
              clientWidth = 750 * dpr
            }
            oFlag = true
            //docEl.style.fontSize = Math.round(40 * (ow / 750)) + 'px';
            const h = ow / 2300
            $("html").css("font-size", new Number(100 * h).toFixed(0) + "px") //设置页面字体大小
            let winHeight = clientHeight > clientWidth ? clientHeight : clientWidth

            //设置游戏页面高度
            if (window.location.href.indexOf("playGame") != -1) {
              let headerHeight = $("#themeLayout header").height()
              $(".wrap").css("height", winHeight + "px")
              $(".wrap").css("margin-top", 0)
              $("#Iframe_Game").css("margin-top", headerHeight + "px")
              $("#Iframe_Game").css("height", winHeight + "px")
            }
            //设置普通页面高度
            else {
              let footerHeight = $("#themeLayout footer").height()
              let bodyHeight = winHeight - footerHeight
              $(".wrap").css("height", bodyHeight + "px")
            }

            // 2019.01.16 竖屏时重置动画活动弹回的高度 及 html动画满屏 SeyoChen
            // const BodyHeight = parseInt($('.banner').height()) + parseInt($('#contLeft').height());
            // $('.index-wrap').css({'height': BodyHeight + 'px'});
            // 2019.01.16 竖屏时重置动画活动弹回的高度 及 html动画满屏 SeyoChen end
          }
          if (orientation === "landscape") {
            // 横屏
            // 2019.01.16 横屏时重置动画活动弹回的高度 及 html动画满屏 SeyoChen
            // const BodyHeight = parseInt($('.banner').height()) + parseInt($('#contLeft').height());
            // $('.index-wrap').css({'height': BodyHeight + 'px'});
            // 2019.01.16 横屏时重置动画活动弹回的高度 及 html动画满屏 SeyoChen end

            if (clientHeight === undefined) return
            if (clientHeight / dpr > 750) {
              clientHeight = 750 * dpr
            }
            lFlag = true
            //docEl.style.fontSize = Math.round(40 * (lh / 750)) + 'px';
            const h = lh / 2600
            $("html").css("font-size", new Number(100 * h).toFixed(0) + "px")
            let winWidth = clientWidth > clientHeight ? clientHeight : clientWidth

            //设置游戏页面高度
            if (window.location.href.indexOf("playGame") != -1) {
              let headerHeight = $("#themeLayout header").height()
              $(".wrap").css("height", winWidth + "px")
              $(".wrap").css("margin-top", 0)
              $("#Iframe_Game").css("margin-top", 0)
              $("#Iframe_Game").css("height", winWidth + "px")
            }
            //设置普通页面高度
            else {
              let footerHeight = $("#themeLayout footer").height()
              let bodyHeight = winWidth - footerHeight
              $(".wrap").css("height", bodyHeight + "px")
            }
            EventBus.$emit("HOME_RIGHT_SLIDER") //首页游戏第二级页面回收
          }
        }
        if (supportOrientation) {
          window.addEventListener(
            "orientationchange",
            function () {
              if (count < 20) {
                count++
                setTimeout(updateOrientation, 100)
              }
            },
            false
          )
        } else {
          //监听resize事件
          window.addEventListener(
            "resize",
            function () {
              if (count < 20) {
                count++
                setTimeout(updateOrientation, 100)
              }
            },
            false
          )
        }
        updateOrientation()
      }
      window.addEventListener("DOMContentLoaded", init, false)
    })(document, window)
  },
  CheckAccount: async function (_that, goback) {
    // 檢查權限:凍結不可進行操作
    EventBus.$emit("GlobalLoadingTrigger", true)
    const permissionData = await CommonService.Comm_CheckPermission()
    EventBus.$emit("GlobalLoadingTrigger", false)
    if (permissionData.Status != BLDef.SysAccountStatus.LOGINED_ENABLED) {
      let notifyData = {
        NotifyMessage: _that.$t("lang.ACCOUNT_FROZEN_TIP1")
      }
      if (goback) {
        notifyData.CloseFunction = () => {
          window.history.go(-1)
        }
      }
      EventBus.$emit("showWarnTip", notifyData)
      return false
    }
    return true
  },
  // 文本文档空格转换
  enterChange(Data) {
    if (Data !== null && Data !== "" && Data !== undefined) {
      const rtn = Data.replace(/\n|\r\n/g, "<br/>")
      return rtn
    } else {
      return
    }
  }
}
