// input 获取焦点
export const SetFooterBar = (type) => {
  var u = navigator.userAgent
  var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) //ios终端
  if (isiOS && type == "hide") {
    $(".mainMenuNew").css({
      display: "none"
    })
  } else {
    $(".mainMenuNew").css({
      display: "flex"
    })
  }
}

// 过滤popup弹框样式UC浏览器兼容  type: true false
export const SetPopupPosition = (type) => {
  // 判断uc浏览器
  if (navigator.userAgent.indexOf("UCBrowser") > -1) {
    if (type) {
      $("#wrapper").css({ overflow: "hidden" })
      let scrollTopNum = $("#wrapper").scrollTop()
      $("#wrapper").scrollTop(0)
      sessionStorage.setItem("PopupScrollTop", scrollTopNum)
    } else {
      $("#wrapper").css({ overflow: "auto" })
      setTimeout(() => {
        // 判断uc浏览器
        $("#wrapper").css({ overflow: "auto" })
        $("#wrapper").animate({ scrollTop: sessionStorage.getItem("PopupScrollTop") }, 500)
      }, 300)
    }
  }
}

// 个人中心代理中心列表判断uc浏览器处理头部
export const setMyHeaderPosition = () => {
  if (navigator.userAgent.indexOf("UCBrowser") > -1) {
    $("header").css({ position: "static" })
    $(".filter-icon").css({ position: "absolute" })
    $(".common-promotion-wrap .van-icon").css({ position: "absolute" })
    $(".common-promotion-wrap").css({ "padding-top": "0" })
    // } else {
    //     $('header').css({ position: 'fixed' });
    //     $('.filter-icon').css({ position: 'fixed' });
    //     $('.common-promotion-wrap .van-icon').css({ position: 'fixed' });
    //     $('.common-promotion-wrap').css({ 'padding-top': '3.5rem' });
  }
}

// 个人中心代理中心列表判断uc浏览器处理头部
export const OpenNewBox = () => {
  if (localStorage.ServeLink) {
    return window.open(
      localStorage.ServeLink,
      "NewWindow",
      "height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no"
    )
  }
}

export default {
  SetFooterBar,
  SetPopupPosition,
  setMyHeaderPosition,
  OpenNewBox
}
