export const scrollToTop = target => {
  const config = {
    top: 0,
    left: 0,
    behavior: "auto"
  }

  const defaultTarget = document.querySelector("#wrapper")

  if (target instanceof Element) {
    target.scroll(config)
  } else {
    defaultTarget.scroll(config)
  }
}

export const selectTextNode = node => {
  const selection = window.getSelection()
  const range = document.createRange()
  range.selectNodeContents(node)
  selection.removeAllRanges()
  selection.addRange(range)
}

export const copyTextNode = node => {
  selectTextNode(node)
  document.execCommand("Copy")
}
