import Vue from "vue"
import App from "@/components/base/base.vue"
import { fetcher } from "@/libs/fetcher.js"
import { store } from "@/store/index.js"
import router from "@/router/index.js"
import i18n from "@/lang/i18nLoader.js"
import { generateImageUrl } from "@/helpers/image.js"
import { isMobileAgent } from "@/libs/navigator.js"

export const mainLoader = () => {
  fetcher({ endpoint: "/static/customer/versionSet.json" }, "GET").then((result) => {
    Vue.prototype.$Environment = result.Environment // 处于的环境 1：SGX 2：正式站 20191008 SeyoChen
    Vue.prototype.$Lang = result.Lang // 语言 0:简体中文 1：繁体中文 2：英文 3：越南语

    Vue.prototype.$MobileVersionHost = document.location.protocol + "//" + document.location.host // 移动端地址
    Vue.prototype.$ResourceURL = document.location.protocol + "//" + document.location.host // Resource地址

    Vue.prototype.$WGUID = result.VersionSet.WGUID // 站长ID

    Vue.prototype.$ThemeName = result.ThemeName // 微投皮肤
    Vue.prototype.$DownloadAPP = result.DownloadAPP // App下载路径

    Vue.prototype.$BKBLUrl = document.location.protocol + result.VersionSet.BKBLUrl // 后台接口地址
    Vue.prototype.$SignalRUrl = document.location.protocol + result.VersionSet.SignalRUrl // Signal地址
    Vue.prototype.$ResourceCDN = document.location.protocol + result.VersionSet.ResourceCDN // CDN资源位置

    store.dispatch("site/setSiteId", result.VersionSet.WGUID)

    const resourceCDN = result.VersionSet.ResourceCDN

    Vue.prototype.getImage = (location, isFromCDN = false) => {
      const config = {
        isFromCDN,
        resourceCDN
      }

      return generateImageUrl(location, config)
    }

    switch (Vue.prototype.$Environment) {
      case "1":
        // 配置文件
        Vue.prototype.$WebVersionHost = document.location.protocol + result.VersionSet.WebVersionHost // PC版地址
        break
      case "2":
        // 配置文件
        Vue.prototype.$WebVersionHost =
          document.location.protocol + "//" + document.location.host.replace("mobile", "www") // PC版地址
        break
      default:
        break
    }

    if (isMobileAgent() === false) {
      location.href = Vue.prototype.$WebVersionHost + location.pathname
      return
    }

    new Vue({
      el: "#app",
      i18n,
      store,
      router,
      template: "<App/>",
      components: {
        App
      }
    })
  })
}
