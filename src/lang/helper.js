/* global SITE_KEY */

import CommUtility from "@/assets/scripts/common/CommUtility.js"

export const languageCodeMap = new Map([
  [0, "cn"],
  [1, "tc"],
  [2, "en"],
  [3, "vn"]
])

export const getLanguageIdByCode = (code) => {
  const [result] = [...languageCodeMap].filter(([, v]) => v === code).map((x) => x[0]) || 0
  return result
}

export const getLanguageCode = (codeId) => {
  const defaultLanguageCode = CommUtility.GetCookie("language")

  let finalLanguageId = 0

  if (defaultLanguageCode && defaultLanguageCode !== "undefined" && defaultLanguageCode !== undefined) {
    finalLanguageId = getLanguageIdByCode(defaultLanguageCode)
  } else if (codeId) {
    finalLanguageId = Number(codeId)
  }

  const result = languageCodeMap.get(finalLanguageId)

  CommUtility.SetCookie("language", result)

  return result
}

export const getLanguageFile = async (fileCode) => {
  let modLang = {}

  if (SITE_KEY !== "undefined") {
    modLang = await import(`@/mod/${SITE_KEY}/lang/${fileCode}.js`)
  }

  const langMessages = await import(`@/lang/i18n/${fileCode}.js`)
  const result = { lang: { ...langMessages.default, ...modLang.default } }

  return result
}
