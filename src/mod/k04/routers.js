import { replaceRoutes } from "@/helpers/router.js"
import mainRoutes from "@/router/modules/main.js"
import webLayout from "@/components/base/baseLayout.vue"
import mainLayout from "@/components/mobile/layout/mainLayout"

import newIndex from "@/mod/k04/components/mobile/index/newIndex.vue"
import register from "@/mod/k04/components/mobile/register/index"
import erawMoneyList from "@/mod/k04/components/mobile/member/erawMoneyList"
import depositList from "@/mod/k04/components/mobile/member/depositList"
import joinUs from "@/mod/k04/components/mobile/JoinUs/index"
import notFound from "@/components/base/notFound.vue"
import weChat from "@/components/base/WeChat.vue"
import memberAccountChange from "@/mod/k04/components/mobile/member/accountChange"

const changes = {
  main: [
    {
      //注册
      name: "register",
      meta: {
        mdex: 1
      },
      path: "register/:Data?",
      component: register
    },
    {
      // 取款记录
      name: "memberErawMoneyList",
      meta: {
        mdex: 2
      },
      path: "member/memberErawMoneyList",
      component: erawMoneyList
    },
    {
      // 存款记录
      name: "memberDepositList",
      meta: {
        mdex: 2
      },
      path: "member/memberDepositList",
      component: depositList
    },
    {
      // 加盟我们
      name: "JoinUs",
      meta: {
        mdex: 1
      },
      path: "JoinUs/:Data",
      component: joinUs
    },
    {
      // 账变记录
      name: "memberAccountChange",
      meta: {
        mdex: 2
      },
      path: "member/memberAccountChange",
      component: memberAccountChange
    }
  ]
}

const adds = {
  main: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]

const result = [
  {
    path: "*",
    component: notFound
  },
  {
    // 微信进入
    name: "WeChatComponent",
    path: "WeChat",
    component: weChat
  },
  {
    path: "/",
    name: "webLayout",
    component: webLayout,
    children: [
      {
        name: "index",
        meta: {
          index: 0
        },
        path: "",
        component: newIndex
      },
      {
        name: "mainLayout",
        meta: {
          index: 3
        },
        path: "",
        component: mainLayout,
        children: [...finalMainRoutes]
      }
    ]
  }
]

export default result
