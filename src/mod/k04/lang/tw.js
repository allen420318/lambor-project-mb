const modMessages = {
  REGISTER_Zalo_TITLE: 'Zalo',
  REGISTER_ENTER_Zalo: '請輸入您的Zalo',
  REGISTER_Zalo_TIP: 'Zalo長度限制2-11',
  ACCOUNT_CHANGE_RECORD: '賬變記錄',

  MEMBER_BETTING_ORDER_NUMBER: '投註單號',
  MEMBER_GAME_NAME: '遊戲名稱',
  MEMBER_DEPOSIT_ORDER_NUMBER: '訂單號',
  MEMBER_DEPOSIT_CASH_FEE: '手續費',
  MEMBER_DEPOSIT_REMARK: '備註',
  MEMBER_TRANSACTION_BALANCE: '交易金額',
}

export default modMessages
