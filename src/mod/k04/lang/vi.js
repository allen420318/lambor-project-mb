const modMessages = {
  REGISTER_Zalo_TITLE: "Zalo",
  REGISTER_ENTER_Zalo: "Vui lòng nhập Zalo của bạn",
  REGISTER_Zalo_TIP: "Giới hạn độ dài Zalo 2-11",
  RECORD_STATUS: "trạng thái",
  MEMBER_BONUS_AMOUNT: "tiền khuyến mãi",
  MEMBER_BETTING_NONE: "truy vấn ghi lục chưa hoàn thành",
  MEMBER_ACTIVITY: "ghi nhận hoạt động",
  MEMBER_BETTING_ORDER_NUMBER: "đơn đặt cược",
  MEMBER_GAME_NAME: "Tên trò chơi",
  MEMBER_DEPOSIT_ORDER_NUMBER: "mã đơn",
  MEMBER_DEPOSIT_CASH_FEE: "phí giao dịch",
  MEMBER_DEPOSIT_REMARK: "Ghi chú",
  MEMBER_TRANSACTION_BALANCE: "đơn vị tiền giao dịch"
}

export default modMessages
