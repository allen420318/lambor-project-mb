const modMessages = {
  REGISTER_Zalo_TITLE: 'Zalo',
  REGISTER_ENTER_Zalo: 'Please enter your Zalo',
  REGISTER_Zalo_TIP: 'Zalo length limit 2-11',
}

export default modMessages
