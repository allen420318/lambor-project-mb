import { replaceRoutes } from "@/helpers/router.js"
import mainRoutes from "@/router/modules/main.js"
import webLayout from "@/mod/k03/components/base/baseLayout.vue"
import mainLayout from "@/components/mobile/layout/mainLayout"
import newIndex from "@/mod/k03/components/mobile/index/newIndex.vue"
import notFound from "@/components/base/notFound.vue"
import weChat from "@/components/base/WeChat.vue"
import recharge from "@/components/mobile/recharge/index"

const changes = {
  main: [
    {
      //充值中心
      name: "recharge",
      meta: {
        mdex: 1
      },
      path: "recharge",
      component: recharge
    },
  ]
}

const adds = {
  main: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]

const result = [
  {
    path: "*",
    component: notFound,
  },
  {
    // 微信进入
    name: "WeChatComponent",
    path: "WeChat",
    component: weChat,
  },
  {
    path: "/",
    name: "webLayout",
    component: webLayout,
    children: [
      {
        name: "index",
        meta: {
          index: 0
        },
        path: "",
        component: newIndex,
      },
      {
        name: "mainLayout",
        meta: {
          index: 3
        },
        path: "",
        component: mainLayout,
        children: [...finalMainRoutes]
      }
    ]
  }
]

export default result
