const modMessages = {
  PROMOTIONAL_ACTIVITY: 'Khuyến mãi',
  ACTIVITY_NOT_SET: 'Trang web chưa cập nhật chương trình khuyến mãi',
  DEPOSIT_ONLINE: 'Thanh toán trực tuyến',
  DEPOSIT_COMPANY: 'Nạp qua thẻ ngân hàng',
  DEPOSIT_ACCOUNT_SITUATION: 'Dựa trên tình hình thực tế tài khoản của bạn ,',
  DEPOSIT_RECOMMENDED: 'hệ thống khuyến nghị các tùy chọn khuyến mãi tốt nhất như sau: ',
  DEPOSIT_NON_PARTICIPATION: 'Không tham gia vào Ưu đãi',
  RECORD_CASH_QUERY: 'Kiểm tra số dư xuất nhập khoản',
  AGENCY_BETTING: 'Kiểm tra kỷ lục đặt cược',
  MEMBER_BETTING_NONE: 'Kiểm tra kỷ lục chưa phát thưởng',
}

export default modMessages
