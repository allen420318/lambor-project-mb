import { replaceRoutes } from "@/helpers/router.js"
import mainRoutes from "@/router/modules/main.js"
import webLayout from "@/components/base/baseLayout.vue"
import newIndex from "@/components/mobile/index/newIndex.vue"

const changes = {
  main: [
    {
      name: "login",
      meta: {
        mdex: 1
      },
      path: "login",
      component: () => import("@/components/mobile/login/index")
    }
  ]
}

const adds = {
  main: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]

const result = [
  {
    path: "*",
    component: () => import("@/components/base/notFound.vue")
  },
  {
    // 微信进入
    name: "WeChatComponent",
    path: "WeChat",
    component: () => import("@/components/base/WeChat.vue")
  },
  {
    path: "/",
    name: "webLayout",
    component: webLayout,
    children: [
      {
        name: "index",
        meta: {
          index: 0
        },
        path: "",
        component: newIndex
      },
      {
        name: "mainLayout",
        meta: {
          index: 3
        },
        path: "",
        component: () => import("@/components/mobile/layout/mainLayout"),
        children: [...finalMainRoutes]
      }
    ]
  }
]

export default result
