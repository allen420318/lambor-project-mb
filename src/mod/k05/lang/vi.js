const modMessages = {
  LONGIN_ENTER_ACCOUNT: "Tên đăng nhập",
  LOGIN_ENTER_PASSWORD: "Mật khẩu",
  REGISTER_ACCOUNT_TITLE: "Tên đăng nhập",
  REGISTER_PHONE_TITLE: "Số điện thoại",
  REGISTER_PASSWORD_TITLE: "Mật khẩu",
  REGISTER_WITHDRAWPWD_TITLE: "Mật mã rút tiền",
  MEMBER_PASSWORD: "Thay đổi mật khẩu",
  PASSWORD_TITLE: "Thay đổi mật khẩu",
  PASSWORD_CONFIRM: "xác nhận mật khẩu"
}

export default modMessages
