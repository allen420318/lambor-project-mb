import { replaceRoutes } from "@/helpers/router.js"
import mainRoutes from "@/router/modules/main.js"
import webLayout from "@/components/base/baseLayout.vue"
import login from "@/components/mobile/login/index"
import newIndex from "@/mod/k05/components/mobile/index/newIndex.vue"
import member from "@/mod/k05/components/mobile/member/index"
import register from "@/mod/k05/components/mobile/register/index"
import nofound from "@/components/base/notFound.vue"
import wechat from "@/components/base/WeChat.vue"
import mainLayout from "@/components/mobile/layout/mainLayout"

const changes = {
  main: [
    {
      name: "login",
      meta: {
        mdex: 1
      },
      path: "login",
      component: login
    },
    {
      //注册
      name: "register",
      meta: {
        mdex: 1
      },
      path: "register/:Data?",
      component: register
    },
    {
      //个人中心
      name: "member",
      meta: {
        mdex: 1
      },
      path: "member",
      component: member
    }
  ]
}

const adds = {
  main: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]

const result = [
  {
    path: "*",
    component: nofound
  },
  {
    // 微信进入
    name: "WeChatComponent",
    path: "WeChat",
    component: wechat
  },
  {
    path: "/",
    name: "webLayout",
    component: webLayout,
    children: [
      {
        name: "index",
        meta: {
          index: 0
        },
        path: "",
        component: newIndex
      },
      {
        name: "mainLayout",
        meta: {
          index: 3
        },
        path: "",
        component: mainLayout,
        children: [...finalMainRoutes]
      }
    ]
  }
]

export default result
