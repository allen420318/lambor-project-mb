const modMessages = {
  REGISTER_ACCOUNT_TITLE: "使用者名称",
  REGISTER_ENTER_ACCOUNT: "请输入使用者名称",
  GAME_LOTTERY: "彩票游戏",
  GAME_POKER: "棋牌对战"
}
export default modMessages
