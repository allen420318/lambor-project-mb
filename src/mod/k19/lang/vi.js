const modMessages = {
  REGISTER_ACCOUNT_TITLE: "Tên đăng nhập",
  REGISTER_ENTER_ACCOUNT: "Vui lòng nhập tên đăng nhập",
  VND_LOTTERY_1: "SỐ ĐỀ Hồ chí minh",
  VND_LOTTERY_2: "Miền Bắc",
  VND_LOTTERY_3: "Miền Trung",
  VND_LOTTERY_4: "Miền Nam",
  VND_LOTTERY_5: "VN 30 giây"
}
export default modMessages
