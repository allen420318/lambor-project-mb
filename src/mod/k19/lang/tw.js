const modMessages = {
  REGISTER_ACCOUNT_TITLE: "使用者名稱",
  REGISTER_ENTER_ACCOUNT: "請輸入使用者名稱",
  GAME_LOTTERY: "彩票遊戲",
  GAME_POKER: "棋牌對戰"
}
export default modMessages
