import { replaceRoutes } from "@/helpers/router.js"
import mainRoutes from "@/router/modules/main.js"
import webLayout from "@/mod/k19/components/base/baseLayout.vue"
import newIndex from "@/mod/k19/components/mobile/index/newIndex.vue"
import login from "@/mod/k19/components/mobile/login/index"
import notfound from "@/components/base/notFound.vue"
import wechat from "@/components/base/WeChat.vue"
import mainlayout from "@/components/mobile/layout/mainLayout"

const changes = {
  main: [
    {
      name: "login",
      meta: {
        mdex: 1
      },
      path: "login",
      component: login
    }
  ]
}

const adds = {
  main: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]

const result = [
  {
    path: "*",
    component: notfound
  },
  {
    // 微信进入
    name: "WeChatComponent",
    path: "WeChat",
    component: wechat
  },
  {
    path: "/",
    name: "webLayout",
    component: webLayout,
    children: [
      {
        name: "index",
        meta: {
          index: 0
        },
        path: "",
        component: newIndex
      },
      {
        name: "mainLayout",
        meta: {
          index: 3
        },
        path: "",
        component: mainlayout,
        children: [...finalMainRoutes]
      }
    ]
  }
]

export default result
