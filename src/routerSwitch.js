/* global SITE_KEY */

import mainRouters from "@/router/modules/main.js"
import webLayout from "@/components/base/baseLayout.vue"
import newIndex from "@/components/mobile/index/newIndex.vue"
import mainLayout from "@/components/mobile/layout/mainLayout.vue"

const defaultRoutes = [
  {
    path: "*",
    component: () => import("@/components/base/notFound.vue")
  },
  {
    // 微信进入
    name: "WeChatComponent",
    path: "WeChat",
    component: () => import("@/components/base/WeChat.vue")
  },
  {
    path: "/",
    name: "webLayout",
    component: webLayout,
    children: [
      {
        name: "index",
        meta: {
          index: 0
        },
        path: "",
        component: newIndex
      },
      {
        name: "mainLayout",
        meta: {
          index: 3
        },
        path: "",
        component: mainLayout,
        children: [...mainRouters]
      }
    ]
  }
]

let configRoutes = {}

if (SITE_KEY === "undefined" || !SITE_KEY) {
  configRoutes = defaultRoutes
} else {
  configRoutes = require(`@/mod/${SITE_KEY}/routers.js`).default
}

export default configRoutes
