import Vue from "vue"
import Vuex from "vuex"
import transferAccount from "./module/transferAccount.js"
import plan from "./module/plan.js"
import deposit from "./module/deposit.js"
import marquee from "./module/marquee.js"
import notice from "./module/notice.js"
import site from "./module/site.js"
import user from "./module/user.js"
import wallet from "./module/wallet.js"
import i18n from "./module/i18n.js"

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    transferAccount,
    plan,
    deposit,
    marquee,
    notice,
    site,
    user,
    wallet,
    i18n
  }
})

export default store
