// Site
export const SET_SITE_BASIC_META = "Set site logo path , name and favicon"
export const SET_SITE_ID = "Set site id"
export const SET_GAME_VENDOR_MAP_LIST = "Set game vendor code mapping list"

// Wallet
export const SET_MAIN_WALLET_BALANCE = "Set main wallet balance"
export const SET_MEMBER_WALLET_BALANCE = "Set agent wallet balance"
export const SET_WALLET_ERROR_COUNTS = "Set actions of wallet error counts"

// Deposit activity
export const SET_CURRENT_ACTIVITY_ID = "Set current activtiy id"

// Game list
export const FETCH_GAME_LIST = "🏄‍♀️ Fetch game list data"

// User
export const FETCH_USER_STATUS = "Fetch user account login status"
export const SET_AGENT_ID = "Set agent id from login data"
export const SET_USER_ID = "Set user id from login data"

// Service
export const INITIALIZE_SERVICES = "🙈 Initialize signalR & login services"

// Language
export const LOAD_LANGUAGE = "⛩🐳 Load localization data from json file."
export const SET_DEFAULT_LANGUAGE_CODE = "⛩🐳 Set default language code."
