export default {
    namespaced: true,
    state: {
        Account: undefined,
        MemberBalance: undefined,
        VAmount: undefined,
        Remark: undefined,
        MemberList: undefined,
        PwdErrCnt: undefined, // 新增取款密码错误次数 2019.12.06 ququ
    },
    getters: {
        getAccount(state) {
            return state.Account;
        },
        getMemberBalance(state) {
            return state.MemberBalance;
        },
        getVAmount(state) {
            return state.VAmount;
        },
        getRemark(state) {
            return state.Remark;
        },
        getMemberList(state) {
            return state.MemberList;
        },
        getPwdErrCnt(state) {
            return state.PwdErrCnt;
        },
    },
    mutations: {
        setAccount(state, val) {
            state.Account = val;
        },
        setMemberBalance(state, val) {
            state.MemberBalance = val;
        },
        setVAmount(state, val) {
            state.VAmount = val;
        },
        setRemark(state, val) {
            state.Remark = val;
        },
        setMemberList(state, val) {
            state.MemberList = val;
        },
        setPwdErrCnt(state, val) {
            state.PwdErrCnt = val;
        },
    },
    actions: {
        setAccountVal: (context, val) => {
            context.commit("setAccount", val);
        },
        setMemberBalanceVal: (context, val) => {
            context.commit("setMemberBalance", val);
        },
        setVAmountVal: (context, val) => {
            context.commit("setVAmount", val);
        },
        setRemarkVal: (context, val) => {
            context.commit("setRemark", val);
        },
        setMemberListVal: (context, val) => {
            context.commit("setMemberList", val);
        },
        setPwdErrCntVal: (context, val) => {
            context.commit("setPwdErrCnt", val);
        },
    }
}
