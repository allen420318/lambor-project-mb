export default {
  namespaced: true,
  state: {
    isFirstEnter: true, // 是否首次进入首页
  },
  getters: {
    getIsFirstEnter(state) {
      return state.isFirstEnter;
    },
  },
  mutations: {
    setIsFirstEnter(state, val) {
      state.isFirstEnter = val;
    },
  },
  actions: {
    setIsFirstEnterVal: (context, val) => {
      context.commit("setIsFirstEnter", val);
    },
  }
}
