const state = {
  listData: []
}

const mutations = {
  setMarqueeChannel(state = {}, payload) {
    state.listData = payload;
  },
}

const actions = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}