import { SET_SITE_BASIC_META, SET_SITE_ID, SET_GAME_VENDOR_MAP_LIST, INITIALIZE_SERVICES } from "../mutation-types.js"
import EventBus from "@/assets/scripts/common/EventBus"

// Platform type code
// Undefined -1 , PC 0 , Mobile 1

const state = {
  name: "",
  logoPath: "",
  favicon: "",
  siteId: "",
  appAgentKey: "LB APP",
  customerServiceLink: "",
  gameVendorMapList: {},
  fixedTimezone: "",
  platformTypeCode: 1,
  isServiceReady: false
}

const actions = {
  setSiteBasicMeta({ commit }, { name, logoPath, favicon }) {
    commit(SET_SITE_BASIC_META, { name, logoPath, favicon })
  },
  setSiteId({ commit }, siteId) {
    commit(SET_SITE_ID, siteId)
  },
  setGameVendorMapList({ commit }, target) {
    const result = Object.fromEntries(Object.entries(target).map(([k, v]) => [k.toUpperCase(), v]))
    commit(SET_GAME_VENDOR_MAP_LIST, result)
  },
  initializeServices({ commit }) {
    // Temporary fixes for login & signalR service
    EventBus.$emit("initializeServices")
    commit(INITIALIZE_SERVICES)
  }
}

const mutations = {
  [SET_SITE_BASIC_META](state, data) {
    state.name = data.name
    state.logoPath = data.logoPath
    state.favicon = data.favicon
  },
  [SET_SITE_ID](state, siteId) {
    state.siteId = siteId
  },
  [SET_GAME_VENDOR_MAP_LIST](state, target) {
    state.gameVendorMapList = target
  },
  [INITIALIZE_SERVICES](state) {
    state.isServiceReady = true
  }
}

const getters = {
  isApp(state) {
    const result = navigator.userAgent.includes(state.appAgentKey)
    return result
  },
  getGameVendorCodeMap(state) {
    return new Map(Object.entries(state.gameVendorMapList))
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
