import { SET_MAIN_WALLET_BALANCE, SET_MEMBER_WALLET_BALANCE, SET_WALLET_ERROR_COUNTS } from "../mutation-types.js"
import { PostAes } from "@/assets/scripts/common/HttpService.js"

const state = {
  mainWalletBalance: null,
  memberWalletBalance: null,
  walletErrorCounts: 0
}

const actions = {
  async fetchMainWalletBalance({ commit }) {
    // ST:4 AT:5001

    const result = await PostAes(4, 5001)

    const {
      Ret,
      Data: { CurrBalance: mainWalletBalance }
    } = result

    if (Ret === 0) {
      commit(SET_MAIN_WALLET_BALANCE, mainWalletBalance)
    }
  },
  async fetchMemberWalletBalance({ commit }) {
    // ST:11 AT:513 LoadMemberBalance_FrontEnd

    const result = await PostAes(11, 513)

    const {
      Ret,
      Data: { MemberBalance: memberWalletBalance, PwdErrCnt: depositPasswordErrorCounts }
    } = result

    if (Ret === 0) {
      commit(SET_MEMBER_WALLET_BALANCE, memberWalletBalance)
      commit(SET_WALLET_ERROR_COUNTS, depositPasswordErrorCounts)
    }
  },
  setMainWalletBalance({ commit }, data) {
    commit(SET_MAIN_WALLET_BALANCE, data)
  },
  setMemberWalletBalance({ commit }, data) {
    commit(SET_MEMBER_WALLET_BALANCE, data)
  },
  setWalletErrorCounts({ commit }, data) {
    commit(SET_WALLET_ERROR_COUNTS, data)
  }
}

const mutations = {
  [SET_MAIN_WALLET_BALANCE](state, data) {
    state.mainWalletBalance = Number(data)
  },
  [SET_MEMBER_WALLET_BALANCE](state, data) {
    state.memberWalletBalance = Number(data)
  },
  [SET_WALLET_ERROR_COUNTS](state, data) {
    state.walletErrorCounts = data
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
