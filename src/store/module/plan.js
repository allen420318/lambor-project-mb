export default {
    namespaced: true,
    state: {
        MemberExtensionQRCode: undefined, // 会员推广二维码
        MemberExtensionUrl: undefined // 会员推广链接
    },
    getters: {
        getMemberExtensionQRCode(state) {
            return state.MemberExtensionQRCode;
        },
        getMemberExtensionUrl(state) {
            return state.MemberExtensionUrl;
        }
    },
    mutations: {
        setMemberExtensionQRCode(state, val) {
            state.MemberExtensionQRCode = val;
        },
        setMemberExtensionUrl(state, val) {
            state.MemberExtensionUrl = val;
        }
    },
    actions: {
        setMemberExtensionQRCodeVal: (context, val) => {
            context.commit("setMemberExtensionQRCode", val);
        },
        setMemberExtensionUrlVal: (context, val) => {
            context.commit("setMemberExtensionUrl", val);
        }
    }
}
