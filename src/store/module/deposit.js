export default {
    namespaced: true,
    state: {
        depositChannel: 0, // 充值渠道
    },
    getters: {
        getDepositChannel(state) {
            return state.depositChannel;
        },
    },
    mutations: {
        setDepositChannel(state, val) {
            state.depositChannel = val;
        },
    },
    actions: {
        setDepositChannelVal: (context, val) => {
            context.commit("setDepositChannel", val);
        },
    }
}
