import { FETCH_USER_STATUS, SET_AGENT_ID, SET_USER_ID } from "../mutation-types.js"
import { PostDynamic } from "@/assets/scripts/common/HttpService.js"
import BLDef from "@/assets/scripts/common/BLDef.js"

const state = {
  userStatus: -1,
  userId: -1,
  agentId: -1
}

const actions = {
  async fetchUserStatus({ commit }) {
    try {
      const {
        ServiceType: st,
        ActType: { Comm_CheckPermission: at }
      } = BLDef.CommonService

      const result = await PostDynamic(st, at)

      if (result.Ret === 0) {
        commit(FETCH_USER_STATUS, result.Status)
      } else {
        throw new Error()
      }
    } catch {
      commit(FETCH_USER_STATUS, -666)
    }
  },
  setAgentId({ commit }) {
    const target = localStorage.getItem("CorrelationIBMemberId")

    if (target) {
      commit(SET_AGENT_ID, Number(target))
    } else {
      commit(SET_AGENT_ID, -1)
    }
  },
  setUserId({ commit }) {
    const target = localStorage.getItem("userId")

    if (target) {
      commit(SET_USER_ID, Number(target))
    } else {
      commit(SET_USER_ID, -1)
    }
  }
}

const mutations = {
  [FETCH_USER_STATUS](state, statusId) {
    state.userStatus = statusId
  },
  [SET_AGENT_ID](state, agentId) {
    state.agentId = agentId
  },
  [SET_USER_ID](state, userId) {
    state.userId = userId
  }
}

const getters = {
  isAgent(state) {
    return state.agentId > 0
  },
  isUserLogin(state) {
    const {
      SysAccountStatus: { LOGINED_ENABLED: isLogin, LOGINED_FROZEN: isLoginFrozen }
    } = BLDef

    if (state.userStatus === isLogin || state.userStatus === isLoginFrozen) {
      return true
    } else {
      return false
    }
  },
  isUserFrozen(state) {
    const {
      SysAccountStatus: { LOGINED_FROZEN: isLoginFrozen }
    } = BLDef

    if (state.userStatus === isLoginFrozen) {
      return true
    } else {
      return false
    }
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
