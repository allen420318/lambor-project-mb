import "jquery"
import Vue from "vue"
import Vuex from "vuex"
import VeeValidate from "vee-validate"
import CommUtility from "@/assets/scripts/common/CommUtility.js"
import VueUtils from "@/assets/scripts/common/VueUtils.js"

import { mainLoader } from "@/helpers/initialize.js"

Vue.use(Vuex)
Vue.use(VeeValidate)

Vue.config.productionTip = false

// tick轉換為本地時間
Vue.filter("utcTicksToLocalTime", function (utcDateTicks, format) {
  return CommUtility.UtcTicksToLocalTime(utcDateTicks, format)
})
Vue.filter("simpleContent", function (content, length) {
  return CommUtility.SimpleContent(content, length)
})
Vue.filter("moneyFormat", function (content) {
  return CommUtility.MoneyFormat(content)
})
Vue.filter("UtcTicksTime", function (utcDateTicks, format) {
  return CommUtility.UtcTicksTime(utcDateTicks, format)
})

Vue.prototype.$VueUtils = VueUtils

mainLoader()

// 画面大小调整时触发计算html font-size
CommUtility.ResetOrientation()
